Funções
-------

A seguir estão documentadas todas as funções...

Simulação
=========

.. autofunction:: simulation.calculate_entanglement_entropy

.. autofunction:: simulation.simulate_with_ancilla



Estabilizadores
===============

.. autofunction:: stabilizers.init_hamiltonian_stabilizers

.. autofunction:: stabilizers.init_z2z2_stabilizers

.. autofunction:: stabilizers.init_topological_stabilizers

.. autofunction:: stabilizers.init_product_stabilizers

.. autofunction:: stabilizers.init_ancilla_product_stabilizers

.. autofunction:: stabilizers.update_stabilizer_mut



Operadores Unitários
====================

.. autofunction:: unitaries.extend_to_unitary

.. autofunction:: unitaries.generate_clifford_unitaries

.. autofunction:: unitaries.select_spt3_unitaries

.. autofunction:: unitaries.apply_3qubit_unitary_mut


Utilidades
==========

.. autofunction:: utils.z2_swap_row_mut

.. autofunction:: utils.z2_swap_col_mut

.. autofunction:: utils.rank_col_echelon_mut

.. autofunction:: utils.commute

.. autofunction:: utils.get_commuting_pivot

.. autofunction:: utils.generate_binary_strings

