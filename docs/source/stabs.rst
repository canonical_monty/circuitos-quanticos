Formalismo de Estabilizadores
-----------------------------

Um elemento do grupo de Pauli sobre \\(n\\) qubits pode ser mapeado em um 
vetores binários :math:`w = (x, z) \in \mathbb{F}_{2}^{n} \times \mathbb{F}_{2}^{n}` 
do seguinte modo:

.. math::

    (x, z)
    \mapsto e^{i\theta}\, \prod_{i = 1}^{n} X_{i}^{x_{i}} \, \prod_{i = 1}^{n} Z_{i}^{z_{i}}.


Devido à estrutura do grupo de Pauli, :math:`\theta \in \mathbb{Z}_{4}`

...



