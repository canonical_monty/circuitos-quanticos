## A DOCUMENTAR!
import numpy as np
import logging

import stabilizers
import utils
import unitaries
import simulation



def main():
    logging.basicConfig(level=logging.INFO)
    #logging.basicConfig(level=logging.DEBUG)

    spt = unitaries.select_spt3_unitaries()
    print(len(spt))

    (stabilizer, _) = stabilizers.init_product_stabilizers(6)
    #unitaries.apply_3qubit_unitary_mut(stabilizer, 2)
    unitaries.apply_3qubit_unitary_mut(stabilizer, spt[0], 2)








if __name__ == "__main__":
    main()


