"""
    Utilidades

    ...

"""

import numpy as np
import logging



def z2_swap_row_mut(M, i, j, colStart = 0):
    """ Troca as linhas ``i`` e ``j`` da matrix ``M``.

    Opcionalmente permuta as linhas somente a partir da coluna ``colStart``.


    Argumentos
    ----------

    M 
        Uma matriz com entradas binárias.

        Tipo: ``np.array(n, m, int8)``


    i 
        Índice da linha a ser trocada.

        Tipo: ``int``


    j
        Índice da linha com a qual se troca.

        Tipo: ``int``


    colStart
        Índice da coluna a partir de qual as linhas são trocadas.

        Tipo: ``int``

    """

    if i != j:
        # Usa a operação XOR para calcular a diferença entre os elementos das
        # colunas e então usa isto para fazer a troca.
        M[i, colStart:] = M[i, colStart:] ^ M[j, colStart:]
        M[j, colStart:] = M[j, colStart:] ^ M[i, colStart:]
        M[i, colStart:] = M[i, colStart:] ^ M[j, colStart:]

    return
    


def z2_swap_col_mut(M, i, j, rowStart = 0):
    """ Troca as colunas ``i`` e ``j`` da matrix ``M``.

    Opcionalmente permuta as linhas somente a partir da linha ``rowStart``.


    Argumentos
    ----------

    M 
        Uma matriz com entradas binárias.

        Tipo: ``np.array(n, m, int8)``


    i 
        Índice da coluna a ser trocada.

        Tipo: ``int``


    j 
        Índice da coluna com a qual se troca.

        Tipo: ``int``


    rowStart
        Índice da linha a partir de qual as colunas são trocadas.

        Tipo: ``int``

    """

    if i != j:
        # Usa a operação XOR para calcular a diferença entre os elementos das
        # linhas e então usa isto para fazer a troca.
        M[rowStart:, i] = M[rowStart:, i] ^ M[rowStart:, j]
        M[rowStart:, j] = M[rowStart:, j] ^ M[rowStart:, i]
        M[rowStart:, i] = M[rowStart:, i] ^ M[rowStart:, j]

    return



def rank_col_echelon_mut(M):
    """
    Calcula o posto da matriz ``M`` e a transforma em uma matriz escalonada por 
    colunas.

    Não vejo motivo para ter usado escalonamento por colunas ao invés de linhas que
    é o usual.

    Argumentos
    ----------

    M
        Uma matriz com entradas binárias.

        Tipo: ``np.array(n, m, int8)``

    """

    (rows, cols) = M.shape
    pivotCol = 0

    for i in range(rows):
        for j in range(pivotCol, cols):
            if M[i, j] != 0:
                for k in range(j + 1, cols):
                    if M[i, k] != 0:
                        ## Zera o elemento (i, k) por meio de subtração de colunas.
                        M[i:, k] = M[i:, k] ^ M[i:, j]


                ## Transporta o primeiro elemento não nulo para a diagonal.
                z2_swap_col_mut(M, j, pivotCol, rowStart = i)
                pivotCol += 1
                break


    return pivotCol



def commute(U, V):
    """
    Verifica se dois operadores de Clifford comutam.

    Argumentos
    ----------

    U
        Um vetor representando um operador de Clifford. Deve ter um número 
        par de elementos, cada qual igual a zero ou um.

        Tipo: ``np.array(2n, int8)``

    V
        Um vetor representando um operador de Clifford. Deve ter um número 
        par de elementos, cada qual igual a zero ou um.

        Tipo: ``np.array(2n, int8)``


    Retorna
    -------

    Um booleano verdadeiro se ``U`` e ``V`` comutarem e falso caso contrário.

    """

    ## TODO: Verificar se os tamanhos são iguais e pares?

    # Os índices menores que ``h`` indicam a presença do operador X
    # enquanto aqueles maiores, a do operador Z.
    h = U.shape[0] // 2

    inner_prod = (np.dot(U[:h], V[h:2*h]) + np.dot(U[h:2*h], V[:h])) % 2
    return inner_prod == 0



def get_commuting_pivot(x_vec, z_vec):
    """
    Os vetores ``x_vec`` e ``z_vec`` sempre devem ter um número par de 
    elementos. Seja :math:`2n` o número de elementos. Esta função, interpretando
    cada vetor como representando um operador de Pauli em :math:`n` sítios 
    e verifica, para cada sítio, se o operador dado em ``x_vec`` comuta com o
    correspondente em ``z_vec`` e retorna o sítio do primeiro par que não comuta.
    Não faço a menor ideia de porque isto é útil.


    Argumentos
    ----------

    x_vec
        Um vetor binário de comprimento par. A princípio representando
        a sequência de operadores :math:`X_{i}`.

        Tipo: ``np.array(2n, int8)``

    z_vec
        Um vetor binário de comprimento par. A princípio representando
        a sequência de operadores :math:`Z_{i}`.

        Tipo: ``np.array(2n, int8)``


    Retorna
    -------

    O índice do sítio para qual o par de operadores dados em ``x_vec`` e ``z_vec``
    não comutam. Retorna ``0.5`` caso nenhum tal par exista.

    .. warning:: 
        Se nenhum par comutante for encontrado deveria resultar em erro. Por hora,
        retorna ``0.5`` que é um valor inválido para índice.
        
    """

    h = x_vec.shape[0] // 2
    for i in range(h):
        if not commute(x_vec[i::h], z_vec[i::h]):
            return i

    # Sinaliza erro.
    logging.error("get_commuting_pivot: Erro!")
    return 0.5



def generate_binary_strings(n: int):
    """
    Gera todas as :math:`2^{n}` sequências binárias de :math:`n` elementos.

    Argumentos
    ----------

    n
        O tamanho das sequências.

        Tipo: ``int``


    Retorna
    -------

    Uma matriz :math:`2^{n} \\times n` contendo, em cada linha, uma das 
    sequências binárias possíveis.

    """

    seq_matrix = np.zeros((2**n, n), dtype = np.int8)
    for i in range(2**n):
        for j in range(n):
            # Como i varia de 0 a 2^n - 1, a representação binária de i
            # passa por todas as sequências binárias de n elementos.
            # Basta extrair cada componente:
            # seq_matrix[i, j] = (i >> j) & 1

            seq_matrix[i, j] = ((i + 1) >> j) & 1
            # seq_matrix[i, j] = (i // 2**(j)) % 2


    return seq_matrix



