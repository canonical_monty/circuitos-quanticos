"""
    Unitárias

    ...

"""


import numpy as np
import logging

from utils import *


# TODO: provavelmente deveria tomar o tamanho dos vetores como argumento também.
def extend_to_unitary(x_vec, z_vec):
    """
    Estende um operador de Pauli para um de Clifford???

    Eu não faço a menor ideia de porque esta função é como é. 
    Não sei qual o propósito de ``get_commuting_pivot`` e nem
    o porquê de ficar verificando comutação.

    Parece que a ideia, no entanto, é, a partir da base de operadores
    X e Z, inserir o operador desejado e completar ela adequadamente
    de algum modo.

    Argumentos
    ----------

    x_vec
        Um vetor binário de comprimento par. A princípio representando
        a sequência de operadores :math:`X_{i}`.

        Tipo: ``np.array(2n, int8)``

    z_vec
        Um vetor binário de comprimento par. A princípio representando
        a sequência de operadores :math:`Z_{i}`.

        Tipo: ``np.array(2n, int8)``


    Retorna
    -------

    Uma unitária de Clifford??

    """

    h = x_vec.shape[0] // 2


    # A princípio esta função pode retornar um resultado inválido, se não houver
    # nenhum par que não comuta. No entanto isso parece não acontecer?????
    # O código original não lida com isso, há apenas um ``print``.
    pivot_idx = get_commuting_pivot(x_vec, z_vec)


    x_basis = np.eye(h, 2*h, dtype=np.int8)
    x_basis[pivot_idx, :] = x_vec
    z2_swap_row_mut(x_basis, 0, pivot_idx)


    z_basis = np.eye(h, 2*h, k = h, dtype=np.int8)
    z_basis[pivot_idx, :] = z_vec
    z2_swap_row_mut(z_basis, 0, pivot_idx)


    for i in range(1, h):

        # Certifica que Z[i] comuta com X[j] para j < i
        for j in range(i):
            if not commute(z_basis[i, :], x_basis[j, :]):
                z_basis[i, :] = z_basis[i, :] ^ z_basis[j, :]


        # Certifica que Z[i] comuta com Z[j] para j < i
        for j in range(i):
            if not commute(z_basis[i, :], z_basis[j, :]):
                z_basis[i, :] = z_basis[i, :] ^ x_basis[j, :]


        # Verifica se algum X[j] não comuta com Z[i] dentre os restantes. Isto
        # é, para i <= j < h:
        has_noncommuting_pair = False
        for j in range(i, h):
            if not commute(z_basis[i, :], x_basis[j, :]):
                z2_swap_row_mut(x_basis, j, i)
                has_noncommuting_pair = True
                break

        # Caso não seja encontrado procure entre os Z's restantes.
        if not has_noncommuting_pair:
            for j in range(i, h):
                if not commute(z_basis[i, :], z_basis[j, :]):
                    # Troca o elemento Z[j] com o correspondente em X[i].
                    z_basis[j, :] = z_basis[j, :] ^ x_basis[i, :]
                    x_basis[i, :] = x_basis[i, :] ^ z_basis[j, :]
                    z_basis[j, :] = z_basis[j, :] ^ x_basis[i, :]
                    
                    has_noncommuting_pair = True
                    break

            if not has_noncommuting_pair:
                logging.error("Nenhum par não comutante encontrado!")


        # Certifica que X[i] comuta com X[j] para j < i
        for j in range(i):
            if not commute(x_basis[i, :], x_basis[j, :]):
                x_basis[i, :] = x_basis[i, :] ^ z_basis[j, :]


        # Certifica que X[i] comuta com Z[j] para j < i
        for j in range(i):
            if not commute(x_basis[i, :], z_basis[j, :]):
                x_basis[i, :] = x_basis[i, :] ^ x_basis[j, :]



    result = np.vstack((x_basis, z_basis))
    return result.T


def generate_clifford_unitaries(n: int):
    """
    Gera todas a unitárias de Clifford em :math:`n` sítios. 

    Não faço a menor ideia de porque esta função é como é para :math:`n > 1`.


    Argumentos
    ----------

    n
        O número de sítios nos quais a unitária atua.

        Tipo: ``int``
        

    Retorna
    -------

    Uma lista contendo todas as unitárias de Clifford para :math:`n` sítios.
        
    """

    if n == 1:
        w = generate_binary_strings(4)
        (rows, cols) = w.shape

        unitaries_list = []
        for i in range(rows):
            # Para um único sítio, um estado possui um único estabilizador e um
            # desestabilizador e eles devem anti-comutar segundo Aaronson e Gottesman. 
            if not commute(w[i, :2], w[i, 2:]):
                u = np.reshape(w[i, :], (2, 2), order = 'F')
                unitaries_list.append(u)

        return unitaries_list
    else:
        unitaries_list = []
        subunitaries_list = generate_clifford_unitaries(n - 1)

        # Essa matriz será usada para formar uma unitária em n sítios
        # a partir das unitárias de n - 1 sítios geradas.
        V = np.zeros((2*n, 2*n), np.int8)
        V[0, 0] = 1
        V[n, n] = 1

        # Índices, para facilitar.
        V_x = slice(1, n)
        V_z = slice(n + 1, 2*n)
        Q_x = slice(0, n - 1)
        Q_z = slice(n - 1, 2*n - 2)


        p = generate_binary_strings(4 * n)
        (rows, cols) = p.shape
        for i in range(rows):
            if not commute(p[i, :2*n], p[i, 2*n:]):
                U = extend_to_unitary(p[i, :2*n], p[i, 2*n:])
                for Q in subunitaries_list:

                    ## Separa em uma função própria.
                    ## Insere a matriz Q na matriz V mantendo as linhas e colunas de
                    ## índice 0 e n intactas. Essencialmente, anexa um sítio a Q.
                    ## V começa com um operador X no primeiro sítio como desestabilzador?
                    ## e um Z no primeiro sítio como estabilizador.

                    ## Igualmente misterioso é o fato que V não é resetada em cada 
                    ## passo do loop mas continuamente sobrescrita. 
                    V[V_x, V_x] = Q[Q_x, Q_x]
                    V[V_x, V_z] = Q[Q_x, Q_z]
                    V[V_z, V_x] = Q[Q_z, Q_x]
                    V[V_z, V_z] = Q[Q_z, Q_z]

                    u = np.matmul(U, V) % 2
                    unitaries_list.append(np.matmul(U, V) % 2)


        return unitaries_list


def select_spt3_unitaries():
    """
    Seleciona, dentre todas as unitárias de três sítios, aquelas que são compatíveis
    com o estado topológico protegido pela simetria :math:`\mathbb{Z}_{2} \\times 
    \mathbb{Z}_{2}`.
    

    Retorna
    -------

    Uma lista com todas as unitárias de três sítios com as propriedades dadas.
    """


    unitaries_list = generate_clifford_unitaries(3)
    spt_unitaries = []


    z2 = np.zeros(6, np.int8)
    z2[4] = 1

    z1_z3 = np.zeros(6, np.int8)
    z1_z3[3] = 1
    z1_z3[5] = 1

    for u in unitaries_list:
        # A princípio acredito que isto checa a presença do operador Z no sítio
        # dois. Mas é estranho que a comparação é feita com as colunas...
        check_z2 = u[:, 4] == z2

        # Não entendo porque esta comparação é assim.
        check_z1_z3 = (u[:, 3] ^ u[:, 5]) == z1_z3

        if check_z2.all() and check_z1_z3.all():
            # Por que transpor?
            spt_unitaries.append(u.T)


    return spt_unitaries



def apply_3qubit_unitary_mut(stabilizer, unitary_T, i):
    """
    Aplica uma unitária de três sítios a um estabilizador.

    A unitária atua nos sítios ``i``, ``i + 1`` e ``i + 2``. A aplicação é feita
    extraindo-se os operadores do estabilizador que atuam nestes sítios, multiplicando
    a matriz resultante pela unitária e então inserindo o resultado de volta no
    estabilizador.

    O estabilizador é modificado diretamente.

    Argumentos
    ----------

    stabilizer
        A matrix de estabilizadores representando o estado atual da cadeia. Deve ter
        no mínimo três sítios.

        Tipo: ``np.array(n, 2n, np.int8)`` com :math:`n \le 3`.


    unitary_T
        A matriz representando o operador unitário a ser aplicado. Aparentemente 
        espera-se que seja fornecido a transposta da unitária a ser aplicada??

        Tipo: ``np.array(6, 3, np.int8)``.


    i
        Índice indicando o sítio a partir da qual a unitária atua.

        Tipo: ``int``.

    """

    ## Número de sítios == número de linhas do estabilizador.
    n = np.shape(stabilizer)[0]

    ## Extrai o estabilizador que atua apenas nos mesmos três sítios que a unitária.
    j = i + n
    sub_stabilizer = stabilizer[:, [i, i + 1, i + 2, j, j + 1, j + 2]]

    sub_stabilizer = np.matmul(sub_stabilizer, unitary_T)
    stabilizer[:, i:(i + 3)] = sub_stabilizer[:, :3]
    stabilizer[:, j:(j + 3)] = sub_stabilizer[:, 3:]
