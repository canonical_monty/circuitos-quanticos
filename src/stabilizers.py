""" Estabilizadores

    ...
"""

import numpy as np


def init_hamiltonian_stabilizers(n: int):
    """ 
    Gera os estabilizadores :math:`X_{i - 1}Z_{i}X_{i + 1}` onde :math:`2 \le i \le n - 1`.
    Isto é, considera-se condições de contorno abertas. Estes estabilizadores correspondem
    aos termos da hamiltoniana.

    
    Argumentos
    ----------
    
    n:
        Número de sítios da rede.

        Tipo: ``int``


    Retorna
    -------

    Uma matriz :math:`(n - 2) \\times 2n` contendo todos os estabilizadores e um vetor de 
    :math:`n - 2` entradas contendo os sinais dos estabilizadores.

    """
    
    x_stab = np.zeros((n - 2, n), dtype=np.int8)
    z_stab = np.zeros((n - 2, n), dtype=np.int8)
    signs = np.zeros(n - 2, dtype=np.int8)

    
    for i in range(n - 2):
        x_stab[i, i] = 1
        z_stab[i, i + 1] = 1
        x_stab[i, i + 2] = 1
        
    return (np.concatenate((x_stab, z_stab), axis=1), signs)



def init_z2z2_stabilizers(n: int):
    """ 
    Gera os dois estabilizadores da simetria :math:`\mathbb{Z}_{2} \\times \mathbb{Z}_{2}`
    dados, respectivamente, pelo produto de operadores :math:`Z` em todos os sítios pares
    e em todos os sítios ímpares.

    Argumentos
    ----------

    n
        O tamanho da rede sob a qual os estabilizadores atuam.

        Tipo: ``int``


    Retorna
    -------

    Uma matriz 2 x 2n contendo todos os estabilizadores e um vetor de 2
    entradas contendo os sinais dos estabilizadores.

    """

    x_stab = np.zeros((2, n), dtype=np.int8)
    z_stab = np.zeros((2, n), dtype=np.int8)
    signs = np.zeros(2, dtype=np.int8)

    
    for i in range(n // 2):
        z_stab[0, 2 * i] = 1
        z_stab[1, (2 * i) + 1] = 1
        
    return (np.concatenate((x_stab, z_stab), axis=1), signs)



def init_topological_stabilizers(n: int):
    """ 
    Gera o conjunto de estabilizadores do estado topológico :math:`\mathbb{Z}_{2}
    \\times \mathbb{Z}_{2}`

    Gera os estabilizadores :math:`X_{i - 1}Z_{i}X_{i + 1}` onde i varia entre 1 e
    n - 1 com condições de contorno abertas e os dois estabilizadores da 
    simetria Z2 x Z2.

    
    Argumentos
    ----------

    n 
        O tamanho da rede sob a qual os estabilizadores atuam.

        Tipo: ``int``


    Retorna
    -------

    Uma matriz n x 2n contendo todos os estabilizadores e um vetor de n
    entradas contendo os sinais dos estabilizadores.

    """
    
    (H_stabs, H_signs) = init_hamiltonian_stabilizers(n)
    (S_stabs, S_signs) = init_z2z2_stabilizers(n)

    stabs = np.vstack((S_stabs[0], H_stabs, S_stabs[1]))
    signs = np.concatenate(([S_signs[0]], H_signs, [S_signs[1]]))

    return(stabs, signs)



def init_product_stabilizers(n: int):
    """ Gera os estabilizadores dos estados produto para uma cadeia de :math:`n` sítios.

    A base dos estados produto é a dos operadores :math:`Z_{i}` para :math:`1 \le i \le n`.

    Argumentos
    ----------

    n
        O tamanho da rede sob a qual os estabilizadores atuam.

        Tipo: ``int``


    Retorna
    -------

    Uma matriz :math:`n \\times 2n` contendo todos os estabilizadores e um vetor de 
    :math:`n` entradas contendo os sinais dos estabilizadores.

    """

    z_stabs = np.eye(n, 2*n, k = n, dtype = np.int8)
    sign = np.zeros(n, dtype = np.int8)

    return (z_stabs, sign)



def init_ancilla_product_stabilizers(n: int):
    """

    Argumentos
    ----------

    n
        O tamanho da rede sob a qual os estabilizadores atuam.

        Tipo: ``int``


    Retorna
    -------

    Uma matriz :math:`n \\times 2n` contendo todos os estabilizadores e um vetor de 
    :math:`n` entradas contendo os sinais dos estabilizadores.

    """

    # Acrescenta um qubit a mais aos estabilizadores produto em ``n`` sítios.
    stabs = np.eye(n + 1, 2*n + 2, k = n + 1, dtype = np.int8)
    stabs[n, 2*n + 1] = 0

    # Inicia os novos estabilizadores: Z_{n/2 - 1}Z_{a}, Z_{n/2 + 1}Z_{a} e
    # X_{n/2 - 1}X_{a}X_{n/2 + 1}.
    h = (n // 2) - 1
    stabs[h - 1, 2*n + 1] = 1
    stabs[h + 1, 2*n + 1] = 1

    stabs[n, h - 1] = 1
    stabs[n, h + 1] = 1
    stabs[n, n] = 1


    sign = np.zeros(n + 1, dtype = np.int8)
    return (stabs, sign)




def update_stabilizer_mut(stabilizer, unitary):
    """

    Atualiza? um estabilizador

    
    Argumentos
    ----------

    stabilizer
        Uma matriz representando um estabilizador.

        Tipo: ``np.array(n, 2n)``

        Quais são as dimensões? Existe restrição?


    unitary
        Um único operador unitário.

        Tipo ``np.array(2n)``


    Retorna
    -------

    ...

    """

    (rows, _) = stabilizer.shape

    unitary_x = unitary[:rows]
    unitary_z = unitary[rows:]

    stabilizer_x = stabilizer[:, :rows]
    stabilizer_z = stabilizer[:, rows:]


    ## Calcula, simultaneamente, o comutador de ``unitary`` com todos os
    ## operadores em ``stabilizer`` e guarda em um vetor.
    commutator_vector = (stabilizer_x * unitary_z + stabilizer_z * unitary_x) % 2

    i = 0
    while i < n:
        if commutator_vector[i] == 0:
            i += 1
        else:
            break

    if i >= n:
        return

    for j in range(i + 1, n):
        if commutator_vector[j] != 0:
            stabilizer_x[j, :] = stabilizer_x[j, :] ^ stabilizer_x[i, :]
            stabilizer_z[j, :] = stabilizer_z[j, :] ^ stabilizer_z[i, :]


    stabilizer_x[i, :] = unitary_x
    stabilizer_z[i, :] = unitary_z

    return
