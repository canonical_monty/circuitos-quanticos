"""
    Simulação propriamente

    ...

"""


import numpy as np
import random as rnd

from stabilizers import *
from utils import *



def calculate_entanglement_entropy(stabilizer, subregion):
    """
    Calcula a entropia de emaranhamento da subregião ``subregion`` do
    estado dado por ``stabilizer``.


    Argumentos
    ----------

    stabilizer
        Uma matriz representando os estabilizadores do estado.

        Tipo: ``np.array(n, 2n, np.int8)``


    subregion
        Um vetor contendo os índices da subregião usada para calcular a entropia.

        Tipo: ``np.array(n, np.int8)``

    """

    n = np.shape(stabilizer)[0]

    indices = np.hstack((subregion, subregion + n))
    ## Note que isto não é uma janela para ``stabilizer``. Consequentemente
    ## ``rank_col_echelon_mut`` não altera o estabilizador.
    partition = stabilizer[:, indices]

    return rank_col_echelon_mut(partition) - indices.size



def simulate_with_ancilla(n_sites, ensemble_size, p_unitary, p_single):

    ## Estabilizadores de três qubits a serem medidos.
    (top_stabs, _) = init_topological_stabilizers(n_sites)
    ## Acrescenta o qubit auxiliar.
    top_stabs = np.hstack(
        (top_stabs[:, :n], np.zeros(n_sites),
        top_stabs[:, n:], np.zeros(n_sites))
    )


    ## Estabilizadores de um qubit a serem medidos.
    (prod_stabs, _) = init_product_stabilizers(n_sites)
    ## Acrescenta o qubit auxiliar.
    prod_stabs = np.hstack(
        (prod_stabs[:, :n], np.zeros(n_sites),
        prod_stabs[:, n:], np.zeros(n_sites))
    )


    ## Partições da cadeia para calcular a entropia de emaranhamento
    L = n_sites // 4
    A = np.arange(0, L)
    B = np.arange(L, 2*L)
    D = np.arange(2*L, 3*L)
    C = np.arange(3*L, n_sites)


    ## Tempo
    duration = 2 * n_sites
    purification_time = n_sites

    
    ## Prealocação dos vetores a serem populados com medidas
    topological_entropy_data = np.zeros((duration, 2), dtype = np.float64)
    quarter_entropy_data = np.zeros((duration, 2), dtype = np.float64)
    half_entropy_data = np.zeros((duration, 2), dtype = np.float64)

    entropy_profile_data = np.zeros((n_sites, 2), dtype = np.float64)
    ancilla_entropy_data = np.zeros((purification_time, 2), dtype = np.float64)
    

    ## Loop de simulação
    for r in range(ensemble_size):

        (stabilizer, _) = init_produc_stabilizers(n_sites + 1)

        for t in range(duration):

            quarter_entropy = calculate_entanglement_entropy(stabilizer, A)
            quarter_entropy_data[t, 0] += quarter_entropy
            quarter_entropy_data[t, 1] += quarter_entropy**2

            half_entropy = calculate_entanglement_entropy(stabilizer, np.hstack((A, B)))
            half_entropy_data[t, 0] += half_entropy
            half_entropy_data[t, 1] += half_entropy**2

            topological_entropy = half_entropy 
                + calculate_entanglement_entropy(stabilizer, np.hstack((B, C)))
                + calculate_entanglement_entropy(stabilizer, B)
                + calculate_entanglement_entropy(stabilizer, D)

            topological_entropy_data[t, 0] += topological_entropy
            topological_entropy_data[t, 1] += topological_entropy**2

            
            for _ in range(n_sites):
                triple_coin = rnd.random()
                if triple_coin <= p_unitary:
                    site = rnd.randint(0, n_sites - 3)
                    apply_3qubit_unitary_mut(
                        stabilizer,
                        unitary_list[rnd.randint(0, unitary_n - 1), 
                        site
                    )
                elif triple_coin <= p_unitary + p_single:
                    site = rnd.randint(0, n_sites - 1)
                    update_stabilizer_mut(stabilizer, prod_stabs[site, :])
                    
                else:
                    site = rnd.randint(0, n_sites - 1)
                    update_stabilizer_mut(stabilizer, top_stabs[site, :])


        for partition_site in range(1, n_sites + 1):
            entanglement_profile = calculate_entanglement_entropy(
                stabilizer,
                np.arange(0, partition_site)
            )

            entropy_profile_data[partition_site, 0] += entanglement_profile
            entropy_profile_data[partition_site, 1] += entanglement_profile**2


        ## Emaranha o qubit auxiliar
        ancilla_stab_1 = np.zeros(2*(n_sites + 1), dtype = np.int8)
        ancilla_stab_2 = np.zeros(2*(n_sites + 1), dtype = np.int8)
        ancilla_stab_3 = np.zeros(2*(n_sites + 1), dtype = np.int8)

        half_minus = n_sites // 2
        half_plus = n_stes // 2

        ## X_{n/2 - 1}X_{A}X_{n/2 + 1}
        ancilla_stab_1[half_minus] = 1
        ancilla_stab_1[half_plus] = 1
        ancilla_stab_1[n_sites + 1] = 1

        ## Z_{n/2 - 1}Z_{A}
        ancilla_stab_2[n_sites + 1 + half_minus] = 1
        ancilla_stab_2[2*(n_sites + 1) - 1] = 1

        ## Z_{A}Z_{n/2 + 1}
        ancilla_stab_3[n_sites + 1 + half_plus] = 1
        ancilla_stab_3[2*(n_sites + 1) - 1] = 1

        update_stabilizer_mut(stabilizer, ancilla_stab_1)
        update_stabilizer_mut(stabilizer, ancilla_stab_2)
        update_stabilizer_mut(stabilizer, ancilla_stab_3)



        for t in range(purification_time):

            ancilla_entropy = calculate_entanglement_entropy(stabilizer, np.array([n_sites]))
            ancilla_entropy_data[t, 0] += ancilla_entropy
            ancilla_entropy_data[t, 0] += ancilla_entropy**2

            for _ in range(n_sites):
                triple_coin = rnd.random()
                if triple_coin <= p_unitary:
                    site = rnd.randint(0, n_sites - 3)
                    apply_3qubit_unitary_mut(
                        stabilizer,
                        unitary_list[rnd.randint(0, unitary_n - 1), 
                        site
                    )
                elif triple_coin <= p_unitary + p_single:
                    site = rnd.randint(0, n_sites - 1)
                    update_stabilizer_mut(stabilizer, prod_stabs[site, :])
                    
                else:
                    site = rnd.randint(0, n_sites - 1)
                    update_stabilizer_mut(stabilizer, top_stabs[site, :])


            
    return (
        topological_entropy_data, 
        quarter_entropy_data,
        half_entropy_data,
        entropy_profile_data,
        ancilla_entropy_data,
    ) 











